# semver

*A tool that bumps you versions, semantically*

## Semantic versioning, in short

*Read more about semantic versioning [here](https://semver.org/).*

Given a version number \<major>.\<minor>.\<patch>, increment the:

* \<major> version when you make incompatible API changes
* \<minor> version when you add functionality in a backwards compatible manner
* \<patch> version when you make backwards compatible bug fixes

`semver` also includes \<build> number which is for things like the RPM "iteration"
that can change when a build is updated with the same source code but with other
changes (documentation, dependencies, flags etc).


## Install

**Remote**

```
> go get gitlab.com/MaxIV/app-maxiv-semver/cmd/semver
```

**From local code**

```
> go install ./cmd/semver
```

**Make sure it works** (remember to check if `~/go/bin` is in `PATH`)

```
> semver --help
```

## Get started

Create the `semver.yml` file.

```
> cd path/to/project
> semver init
```

This creates an initial config that will keep the `semver.yml` file up to date. Use this example to create more update paths.

The simplest way when adding the `pattern` to the `semver.yml` file, is to copy the full line in the file where you store the (or part of) version. Then, for each number, replace it with the correct template part (use the config for `semver.yml` as an example).

### Example of config file

```yaml
version: 1.1.0-1
files:
- path: semver.yml
  pattern: 'version: {{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}'
- path: setup.py
  pattern: 'version="{{.Major}}.{{.Minor}}.{{.Patch}}"'
- path: .gitlab-ci.yml
  pattern: '--iteration {{.Build}}'
- path: project/version.py
  pattern: 'CURRENT_VERSION = "{{.Major}}.{{.Minor}}.{{.Patch}}"'

```

### Bumps

There's a bump type for each part of the version.

```
> semver bump <major|minor|patch|build>
> semver bump --help # shows more information about this
```
