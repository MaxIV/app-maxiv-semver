package semver

import (
	"testing"

	check "gitlab.com/MaxIV/lib-maxiv-go-check"
)

func TestFormatVersion(t *testing.T) {
	type row struct {
		tpl      string
		version  Version
		expected string
		err      bool
	}

	table := []row{
		{
			tpl:      "{{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}",
			version:  Version{1, 2, 3, 4},
			expected: "1.2.3-4",
			err:      false,
		},
		{
			// Malformed template
			tpl:      "malformed-{{.template",
			version:  Version{1, 2, 3, 4},
			expected: "",
			err:      true,
		},
		{
			// Good template but missing referenced value
			tpl:      "{{.nonExistingKey}}",
			version:  Version{1, 2, 3, 4},
			expected: "",
			err:      true,
		},
	}

	for _, row := range table {
		res, err := row.version.Format(row.tpl)
		check.Assert(t, (err == nil) != row.err, "expected error to be present: %t", row.err)
		check.Equals(t, row.expected, res)
	}

}

func TestVersionStringToVersion(t *testing.T) {
	type row struct {
		input      string
		defenition string
		expected   Version
		err        bool
	}

	table := []row{
		{
			input:    "",
			expected: Version{},
			err:      true,
		},
		{
			input:    "asdfa",
			expected: Version{},
			err:      true,
		},
		{
			input:    "-1.0.0-0",
			expected: Version{},
			err:      true,
		},
		{
			input:    "0.0.0-0",
			expected: Version{},
			err:      true,
		},
		{
			input:    "0.0.0-1",
			expected: Version{0, 0, 0, 1},
			err:      false,
		},
		{
			input:    "5823.12837.28372837-0",
			expected: Version{0, 0, 0, 1},
			err:      true,
		},
		{
			input:    "5823.12837.28372837-1",
			expected: Version{5823, 12837, 28372837, 1},
			err:      false,
		},
		{
			input:    "a.0.0-1",
			expected: Version{},
			err:      true,
		},
		{
			input:    "0.a.0-1",
			expected: Version{},
			err:      true,
		},
		{
			input:    "0.0.a-1",
			expected: Version{},
			err:      true,
		},
		{
			input:    "0.0.0-a",
			expected: Version{},
			err:      true,
		},
		{
			input:    "bad.format-1283.sdkfj",
			expected: Version{},
			err:      true,
		},
	}

	for _, row := range table {
		v, err := NewVersionFromString(row.input)
		check.Assert(t, (err == nil) != row.err, "expected error to be present: %t", row.err)
		if err == nil {
			check.Equals(t, row.expected, v)
		}
	}
}

func TestVersionValidation(t *testing.T) {
	type row struct {
		version Version
		err     bool
	}

	table := []row{
		{
			version: Version{1, 0, 0, 1},
			err:     false,
		},
		{
			version: Version{},
			err:     true,
		},
		{
			version: Version{0, 0, 0, 0},
			err:     true,
		},
		{
			version: Version{0, 0, 0, 1},
			err:     false,
		},
	}

	for i, row := range table {
		err := row.version.Validate()
		check.Assert(t, (err == nil) != row.err, "%d: expected error to be present: %t", i, row.err)
	}
}

func TestBump(t *testing.T) {
	type row struct {
		version  Version
		bump     Bump
		expected Version
	}

	table := []row{
		// Regular bumps
		{
			version:  Version{1, 0, 0, 1},
			bump:     BumpBuild,
			expected: Version{1, 0, 0, 2},
		},
		{
			version:  Version{1, 0, 0, 1},
			bump:     BumpPatch,
			expected: Version{1, 0, 1, 1},
		},
		{
			version:  Version{1, 0, 0, 1},
			bump:     BumpMinor,
			expected: Version{1, 1, 0, 1},
		},
		{
			version:  Version{1, 0, 0, 1},
			bump:     BumpMajor,
			expected: Version{2, 0, 0, 1},
		},
		// Bumps that reset 'downstream' numbers
		{
			version:  Version{1, 1, 1, 2},
			bump:     BumpPatch,
			expected: Version{1, 1, 2, 1},
		},
		{
			version:  Version{1, 1, 1, 1},
			bump:     BumpMinor,
			expected: Version{1, 2, 0, 1},
		},
		{
			version:  Version{1, 1, 1, 1},
			bump:     BumpMajor,
			expected: Version{2, 0, 0, 1},
		},
	}

	for _, row := range table {
		row.version.Bump(row.bump)
		check.Equals(t, row.expected, row.version)
	}
}

func TestString(t *testing.T) {
	v := NewVersion()
	check.Equals(t, "0.0.0-1", v.String())
}
