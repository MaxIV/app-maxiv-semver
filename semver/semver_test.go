package semver

import (
	"os"
	"testing"

	check "gitlab.com/MaxIV/lib-maxiv-go-check"
)

var simplePythonFileBefore = `
version = "1.0.0"
`

var simplePythonFileAfter = `
version = "2.0.0"
`

var specFileBefore = `
Summary:        Andor (SDK3) streamer residing on DCU.
Name:           dcu-maxiv-andor3
Version:        2.2.0
Release:	    6%{?dist}
License: 	    GPL
`

var specFileAfter = `
Summary:        Andor (SDK3) streamer residing on DCU.
Name:           dcu-maxiv-andor3
Version:        2.2.0
Release:	    7%{?dist}
License: 	    GPL
`

var semverFileBefore = `
version: 0.0.0-1
files:
- path: semver.yml
  pattern: 'version: {{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}'
`

var semverFileAfter = `
version: 1.0.0-1
files:
- path: semver.yml
  pattern: 'version: {{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}'
`

func TestReplacer(t *testing.T) {
	type row struct {
		version  Version
		bump     Bump
		file     string
		pattern  string
		expected string
		err      bool
	}

	table := []row{
		{
			version:  Version{1, 0, 0, 1},
			bump:     BumpMajor,
			file:     simplePythonFileBefore,
			pattern:  "version = \"{{.Major}}.{{.Minor}}.{{.Patch}}\"",
			expected: simplePythonFileAfter,
			err:      false,
		},
		{
			version: Version{2, 2, 0, 6},
			bump:    BumpBuild,
			file:    specFileBefore,
			pattern: "Release:	    {{.Build}}%{?dist}",
			expected: specFileAfter,
			err:      false,
		},
		{
			version:  Version{0, 0, 0, 1},
			bump:     BumpMajor,
			file:     semverFileBefore,
			pattern:  "version: {{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}",
			expected: semverFileAfter,
			err:      false,
		},
	}

	s := semver{}

	for _, row := range table {
		bumped := row.version
		bumped.Bump(row.bump)

		res, err := s.replace(row.version, bumped, row.file, row.pattern)

		check.OK(t, err)
		check.Equals(t, row.expected, res)
	}
}

func TestColoredOutput(t *testing.T) {
	s := semver{}

	// Sets 'TERM' to nothing to have no assumptions in tests
	os.Setenv("TERM", "")

	// User did not pass in --no-color
	check.Equals(t, s.useColors(false), true)

	// User passed in --no-color
	check.Equals(t, s.useColors(true), false)

	// User did not pass in --no-color but got env var set
	os.Setenv("NO_COLOR", "value-not-important")
	check.Equals(t, s.useColors(false), false)
	os.Setenv("NO_COLOR", "")

	// User did not pass in --no-color but got dump terminal
	os.Setenv("TERM", "dumb")
	check.Equals(t, s.useColors(false), false)
	os.Setenv("TERM", "")
}
