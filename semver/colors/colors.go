package colors

import (
	"fmt"

	"github.com/logrusorgru/aurora"
)

var au = aurora.NewAurora(true)

func SetColoring(b bool) {
	au = aurora.NewAurora(b)
}

func Red(str string, parts ...interface{}) string {
	return au.Red(fmt.Sprintf(str, parts...)).String()
}

func Green(str string, parts ...interface{}) string {
	return au.Green(fmt.Sprintf(str, parts...)).String()
}
