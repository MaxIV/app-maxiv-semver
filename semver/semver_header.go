package semver

import "strings"

var header = strings.TrimSpace(`
# Welcome to 'semver'. The tool to help you keep track of versions all across
# your project. The version must be specified with four parts in the following
# format: <major>.<minor>.<patch>-<build>. However, all parts must not be
# present in each place. Use the Golang template rules to tell how you want the
# version to be present in each file.
#
# Link for more info: https://gitlab.com/MaxIV/app-maxiv-semver
`)
