package semver

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"gitlab.com/MaxIV/app-maxiv-semver/semver/colors"
	"gopkg.in/yaml.v2"
)

var defaultConfigFileName = "semver.yml"

type semver struct {
	workingDirectory string
	ConfigFileName   string
	Verbose          bool
}

func NewSemver(workingDirectory string) semver {
	return semver{
		workingDirectory: workingDirectory,
		ConfigFileName:   defaultConfigFileName,
	}
}

// SetColoredOutput gives the ability to toggle colored output
func (self semver) SetColoredOutput(userDefinedNoColors bool) {
	colors.SetColoring(self.useColors(userDefinedNoColors))
}

// useColors decides based on env vars if semver will use colored
// output or not
func (self semver) useColors(userDefinedNoColors bool) bool {
	var useColors = true

	if os.Getenv("NO_COLOR") != "" {
		useColors = false
	}

	if strings.Contains(os.Getenv("TERM"), "dumb") {
		useColors = false
	}

	if userDefinedNoColors {
		useColors = false
	}

	return useColors
}

func (self semver) Init() error {
	// Build the default config
	conf := config{
		Version: NewVersion().String(),
		Files: []configFile{
			configFile{
				Path:    self.ConfigFileName,
				Pattern: "version: {{.Major}}.{{.Minor}}.{{.Patch}}-{{.Build}}",
			},
		},
	}

	// Build path
	p := path.Join(self.workingDirectory, self.ConfigFileName)

	// Make sure file does not exist already
	_, err := os.Stat(p)
	if os.IsNotExist(err) == false {
		return fmt.Errorf("%s already exists", self.ConfigFileName)
	}

	// Create file
	f, err := os.Create(p)
	if err != nil {
		return err
	}

	// Write header (coment)
	header = strings.TrimSpace(header) + "\n\n"
	f.WriteString(header)

	// Write the default config
	enc := yaml.NewEncoder(f)
	err = enc.Encode(conf)
	if err != nil {
		return err
	}

	return nil
}

func (self semver) Bump(bump Bump) error {
	conf, err := self.readConfig()
	if err != nil {
		return err
	}

	currentVersion, err := NewVersionFromString(conf.Version)
	if err != nil {
		return err
	}

	newVersion := currentVersion
	newVersion.Bump(bump)

	for _, confFile := range conf.Files {
		self.vlog("processing %s...", confFile.Path)
		contentBytes, err := ioutil.ReadFile(confFile.Path)
		if err != nil {
			self.elog(err)
			continue
		}

		contents, err := self.replace(currentVersion, newVersion, string(contentBytes), confFile.Pattern)
		if err != nil {
			self.vlog(colors.Red("ERROR\n"))
			self.elog(fmt.Errorf("in file %s: %w", confFile.Path, err))
			continue
		}

		err = ioutil.WriteFile(confFile.Path, []byte(contents), 0644)
		if err != nil {
			self.vlog(colors.Red("ERROR\n"))
			self.elog(err)
			continue
		}
		self.vlog("...%s", colors.Green("OK\n"))
	}

	return nil
}

func (self semver) Check() error {
	conf, err := self.readConfig()
	if err != nil {
		return err
	}

	version, err := NewVersionFromString(conf.Version)
	if err != nil {
		return err
	}

	for _, confFile := range conf.Files {
		self.vlog("processing %s...", confFile.Path)
		contentBytes, err := ioutil.ReadFile(confFile.Path)
		if err != nil {
			self.vlog(colors.Red("ERROR\n"))
			self.elog(err)
			continue
		}

		format, err := version.Format(confFile.Pattern)
		if err != nil {
			self.vlog(colors.Red("ERROR\n"))
			self.elog(fmt.Errorf("could not build format: %w", err))
			continue
		}

		if strings.Index(string(contentBytes), format) == -1 {
			self.vlog(colors.Red("ERROR\n"))
			self.elog(fmt.Errorf("version string '%s' not found in %s", format, confFile.Path))
			continue
		}

		self.vlog(colors.Green("OK\n"))
	}

	return nil

}

// -----------------------------------
// Private helpers

func (self semver) readConfig() (config, error) {
	var conf config
	configFilePath := path.Join(self.workingDirectory, self.ConfigFileName)

	f, err := os.Open(configFilePath)
	if err != nil {
		return conf, err
	}

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&conf)
	if err != nil {
		return conf, err
	}

	return conf, nil
}

func (self semver) replace(oldVersion, newVersion Version, fileContents, pattern string) (string, error) {
	var err error
	search, err := oldVersion.Format(pattern)
	if err != nil {
		return "", err
	}

	replace, err := newVersion.Format(pattern)
	if err != nil {
		return "", err
	}

	// If there's going to be no replacing, no need to do anything.
	if search == replace {
		self.vlog("skipping, no change")
		return fileContents, nil
	}

	self.vlog("replace '%s' with '%s'", search, replace)
	replacedContent := strings.Replace(fileContents, search, replace, -1)
	if replacedContent == fileContents {
		return "", fmt.Errorf("could not find '%s'", search)
	}

	return replacedContent, nil
}

// vlog is a wrapper for verbose logging
func (self semver) vlog(format string, vars ...interface{}) {
	if self.Verbose == false {
		return
	}

	fmt.Printf(format, vars...)
}

// elog is a wrapper for error logs
func (self semver) elog(err error) {
	fmt.Printf("%s: %s\n", colors.Red("ERROR"), err.Error())
}
