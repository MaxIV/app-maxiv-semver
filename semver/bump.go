package semver

import (
	"fmt"
	"strings"
)

type Bump string

const (
	BumpMajor Bump = "major"
	BumpMinor Bump = "minor"
	BumpPatch Bump = "patch"
	BumpBuild Bump = "build"
)

func NewBumpFromString(str string) (Bump, error) {
	str = strings.ToLower(str)
	switch Bump(str) {
	case BumpMajor:
		return BumpMajor, nil
	case BumpMinor:
		return BumpMinor, nil
	case BumpPatch:
		return BumpPatch, nil
	case BumpBuild:
		return BumpBuild, nil
	}

	return BumpMajor, fmt.Errorf("%s is not a valid Bump type", str)
}
