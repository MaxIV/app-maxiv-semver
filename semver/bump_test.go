package semver

import (
	"testing"

	check "gitlab.com/MaxIV/lib-maxiv-go-check"
)

func TestNewBumpFromString(t *testing.T) {
	type row struct {
		input string
		bump  Bump
		error bool
	}

	table := []row{
		{"MaJOR", BumpMajor, false},
		{"MAJOR", BumpMajor, false},
		{" major", BumpMajor, true},
		{"minor", BumpMinor, false},
		{"PaTCH", BumpPatch, false},
		{"builD", BumpBuild, false},
	}

	var b Bump
	var err error
	for _, row := range table {
		b, err = NewBumpFromString(row.input)
		if row.error {
			check.NotOK(t, err, "row")
		} else {
			check.OK(t, err)
		}

		check.Equals(t, b, row.bump)
	}
}
