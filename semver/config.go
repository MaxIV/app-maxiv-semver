package semver

type config struct {
	Version string       `yaml:"version"`
	Files   []configFile `yaml:"files"`
}

type configFile struct {
	Path    string `yaml:"path"`
	Pattern string `yaml:"pattern"`
}
