package semver

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"text/template"
)

type Version struct {
	Major uint
	Minor uint
	Patch uint
	Build uint
}

func (self *Version) Bump(bumpType Bump) {
	switch bumpType {
	case BumpMajor:
		self.Major += 1
		self.Minor = 0
		self.Patch = 0
		self.Build = 1
	case BumpMinor:
		self.Minor += 1
		self.Patch = 0
		self.Build = 1
	case BumpPatch:
		self.Patch += 1
		self.Build = 1
	case BumpBuild:
		self.Build += 1
	}
}

func (self Version) String() string {
	return fmt.Sprintf("%d.%d.%d-%d", self.Major, self.Minor, self.Patch, self.Build)
}

func (self Version) Format(format string) (string, error) {
	tpl, err := template.New("version").Parse(format)
	if err != nil {
		return "", fmt.Errorf("could not format version according to format %s: %w", format, err)
	}

	var buf bytes.Buffer
	err = tpl.Execute(&buf, self)
	if err != nil {
		return "", fmt.Errorf("could not execute the formatting: %w", err)
	}

	return buf.String(), nil
}

func (self Version) Validate() error {
	parts := []string{}

	if self.Build < 1 {
		parts = append(parts, "build cannot be < 1")
	}

	if len(parts) > 0 {
		return fmt.Errorf("%s", strings.Join(parts, ", "))
	}

	return nil
}

func NewVersion() Version {
	return Version{0, 0, 0, 1}
}

func NewVersionFromString(str string) (Version, error) {
	var v Version
	var err error
	var majorRaw, minorRaw, patchRaw, buildRaw string

	str = strings.TrimSpace(str)
	if str == "" {
		return v, fmt.Errorf("version cannot be build from empty string")
	}

	parts := strings.Split(str, "-")
	if len(parts) != 2 {
		return v, fmt.Errorf("expected format 'major.minor.patch-build'")
	}

	buildRaw = parts[1]

	parts = strings.Split(parts[0], ".")
	if len(parts) != 3 {
		return v, fmt.Errorf("expected format 'major.minor.patch-build'")
	}

	majorRaw, minorRaw, patchRaw = parts[0], parts[1], parts[2]

	var i int
	i, err = strconv.Atoi(majorRaw)
	if err != nil {
		return v, fmt.Errorf("major part could not be turned to int: %w", err)
	}
	v.Major = uint(i)

	i, err = strconv.Atoi(minorRaw)
	if err != nil {
		return v, fmt.Errorf("minor part could not be turned to int: %w", err)
	}
	v.Minor = uint(i)

	i, err = strconv.Atoi(patchRaw)
	if err != nil {
		return v, fmt.Errorf("patch part could not be turned to int: %w", err)
	}
	v.Patch = uint(i)

	i, err = strconv.Atoi(buildRaw)
	if err != nil {
		return v, fmt.Errorf("build part could not be turned to int: %w", err)
	}
	v.Build = uint(i)

	err = v.Validate()
	if err != nil {
		return v, err
	}

	return v, nil
}
