# 1.3.3

* Changes colors to the standard Red and Green, instead of the less standard
  BrightRed and BrightGreen.

# 1.3.2

* Fixes dependency issues when installing from local code.

# 1.3.0

* Adds support for `--no-color` flag. It defaults to `$TERM` being set
  to `dumb`, and `$NO_COLOR` being set. User can always override.

# 1.2.0

* Adds the `check` sub command, which validates that the current
  version exists in the format and places that the `semver.yml` file
  dictates. It's useful when adding `semver` to an existing project,
  to make sure each version string is found in the expected place
  without having to use `bump`.
* Adds some color to the output


# 1.1.0

* Support for multiple occurrences of the same version string in one
  file.


# 1.0.0

* Initial release of `semver`
