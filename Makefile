cover:
	go test -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out -o coverage.html
	open coverage.html
	sleep 2
	rm coverage.out coverage.html

install:
	go install ./cmd/semver
