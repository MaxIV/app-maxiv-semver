package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/MaxIV/app-maxiv-semver/semver"
	cli "gitlab.com/MaxIV/lib-maxiv-go-cli"
)

// Managed by semver
const version = "1.3.2"

func main() {
	var verbose bool
	var noColor bool

	app := cli.New("semver")

	// ----------------------------
	// version
	subVersion := app.AddSubCommand("version", "Display version")
	subVersion.Action = func([]string) error {
		fmt.Printf("Version: %s\n", version)
		return nil
	}

	// ----------------------------
	// init
	subInit := app.AddSubCommand("init", "Generate a new config")
	subInit.Action = func([]string) error {
		dir, err := os.Getwd()
		if err != nil {
			return err
		}
		app := semver.NewSemver(dir)
		app.SetColoredOutput(noColor)
		app.Verbose = verbose
		return app.Init()
	}

	// ----------------------------
	// bump
	description := []string{
		"Bumps a part of the version.",
		"Bumping anything bigger than 'build' will reset the lower",
		"parts to defaul values (bump major on 1.2.3-4 -> 2.0.0-1).",
	}
	subBump := app.AddSubCommand("bump", strings.Join(description, "\n"))
	subBump.Arguments = []cli.Argument{
		cli.Argument{
			Name:        "part",
			Description: "which part to bump. Valid values are 'major', 'minor', 'patch', 'build'",
		},
	}
	subBump.FlagSet.BoolVar(&verbose, "verbose", false, "Verbose output")
	subBump.FlagSet.BoolVar(&noColor, "no-color", false, "No color in output")
	subBump.Action = func(args []string) error {
		bump, err := semver.NewBumpFromString(args[0])
		if err != nil {
			return err
		}

		dir, err := os.Getwd()
		if err != nil {
			return err
		}
		app := semver.NewSemver(dir)
		app.SetColoredOutput(noColor)
		app.Verbose = verbose

		return app.Bump(bump)
	}

	// --------------------
	// check
	subCheck := app.AddSubCommand("check", "Checks that the current version exists in all files specified in the config")
	subCheck.FlagSet.BoolVar(&verbose, "verbose", false, "Verbose output")
	subCheck.FlagSet.BoolVar(&noColor, "no-color", false, "No color in output")
	subCheck.Action = func([]string) error {
		dir, err := os.Getwd()
		if err != nil {
			return err
		}

		app := semver.NewSemver(dir)
		app.SetColoredOutput(noColor)
		app.Verbose = verbose

		return app.Check()
	}

	os.Exit(app.Run())
}
