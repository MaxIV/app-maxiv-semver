module gitlab.com/MaxIV/app-maxiv-semver

go 1.15

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	gitlab.com/MaxIV/lib-maxiv-go-check v1.0.1
	gitlab.com/MaxIV/lib-maxiv-go-cli v1.0.1
	gopkg.in/yaml.v2 v2.3.0
)
